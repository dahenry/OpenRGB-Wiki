# N5312A mouse

 This controller should work with all mouse with this chip.
Identified devices that work with this controller: ANT Esports KM540 Mouse,
Marvo M115

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `4E53` | `5406` | N5312A USB Optical Mouse |
