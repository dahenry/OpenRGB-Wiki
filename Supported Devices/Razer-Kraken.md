# Razer Kraken

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1532` | `0501` | Razer Kraken 7.1 |
| `1532` | `0506` | Razer Kraken 7.1 |
| `1532` | `0504` | Razer Kraken 7.1 Chroma |
| `1532` | `0510` | Razer Kraken 7.1 V2 |
| `1532` | `0527` | Razer Kraken Ultimate |
