# Gigabyte RGB Fusion 2 USB

 The Fusion 2 USB controller applies to most AMD and
Intel mainboards from the x570 and z390 chipsets onwards.

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `048D` | `8297` | Gigabyte RGB Fusion 2 USB |
| `048D` | `5702` | Gigabyte RGB Fusion 2 USB |
