# Lian Li Strimer L Connect

 The Lian Li Strimer L Connect `Direct` mode stutters at high frame rates and
and has been rate limited to ~10FPS.

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is problematic (See device page for details)

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0CF2` | `A200` | Strimer L Connect |
