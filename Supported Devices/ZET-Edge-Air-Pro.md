# ZET Edge Air Pro

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `25A7` | `FA3F` | ZET GAMING Edge Air Pro (Wireless) |
| `25A7` | `FA40` | ZET GAMING Edge Air Pro |
| `25A7` | `FA48` | ZET GAMING Edge Air Elit (Wireless) |
| `25A7` | `FA49` | ZET GAMING Edge Air Elit |
