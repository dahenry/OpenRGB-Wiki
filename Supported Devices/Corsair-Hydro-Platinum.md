# Corsair Hydro Platinum

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1B1C` | `0C18` | Corsair Hydro H100i Platinum |
| `1B1C` | `0C19` | Corsair Hydro H100i Platinum SE |
| `1B1C` | `0C17` | Corsair Hydro H115i Platinum |
| `1B1C` | `0C29` | Corsair Hydro H60i Pro XT |
| `1B1C` | `0C20` | Corsair Hydro H100i Pro XT |
| `1B1C` | `0C2D` | Corsair Hydro H100i Pro XT v2 |
| `1B1C` | `0C21` | Corsair Hydro H115i Pro XT |
| `1B1C` | `0C22` | Corsair Hydro H150i Pro XT |
