# MSI GL66 Mystic Light Keyboard (64 Byte)

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1462` | `1562` | MSI Mystic Light MS_1562 |
| `1462` | `1563` | MSI Mystic Light MS_1563 |
| `1462` | `1564` | MSI Mystic Light MS_1564 |
