# Holtek A070

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `04D9` | `A070` | Holtek USB Gaming Mouse |
