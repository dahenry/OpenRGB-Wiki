# Ducky Keyboard

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `04D9` | `0348` | Ducky Shine 7/Ducky One 2 RGB |
| `04D9` | `0356` | Ducky One 2 RGB TKL |
